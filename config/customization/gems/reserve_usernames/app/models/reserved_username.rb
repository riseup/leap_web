require 'active_resource'

class ReservedUsername < ActiveResource::Base
  self.include_root_in_json = false

  self.site = "%s://%s:%s@%s:%s" % [
    ReserveUsernames::CONFIG['api_proto'],
    ReserveUsernames::CONFIG['api_username'],
    ReserveUsernames::CONFIG['api_password'],
    ReserveUsernames::CONFIG['api_host'],
    ReserveUsernames::CONFIG['api_port']
  ]
  self.element_name = ReserveUsernames::CONFIG['api_path']

end
