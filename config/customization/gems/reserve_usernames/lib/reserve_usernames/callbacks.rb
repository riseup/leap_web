module ReserveUsernames::Callbacks
  def self.included(base)
    base.class_eval do

      around_create  :create_reservation
      around_destroy :destroy_reservation

      protected

      def create_reservation
        if username
          Rails.logger.info("ReserveUsernames - creating reservation for username '#{username}'.")
          ru = ReservedUsername.create(:username => username, :reserved_by => owner_string)
          ru.encode
          unless ru.persisted?
            if has_errors?(ru)
              Rails.logger.error("ReserveUsernames - failed to reserve username (#{ru.errors.error_messages}).")
              return false
            else
              Rails.logger.error("ReserveUsernames - failed to reserve username (remote error).")
              raise RuntimeError.new('Failed to reserve username. (remote username reservation error)')
            end
          end
        end
        yield
        if self.persisted?
          ru.confirmed = true
          ru.save
          return false if has_errors?(ru)
        end
      end

      def destroy_reservation
        ru = ReservedUsername.find(username)
        ru.deleted = true
        ru.save
        yield
        if self.destroyed?
          ReservedUsername.delete(username)
        end
      rescue ActiveResource::ResourceNotFound
        yield # object is never deleted if yield is not called.
      end

      def username
        address ? address.split('@').first : nil
      end

      #
      # a label to identify the owner
      #
      def owner_string
        if user && user.id
          user.id
        else
          username
        end
      end

      def has_errors?(record)
        if record.errors.any?
          record.errors.each do |attr, msg|
            Rails.logger.error("ReserveUsernames - REMOTE ERROR #{attr}: #{msg}")
            self.errors.add(attr, msg + " (remote username reservation error)")
          end
          true
        else
          false
        end
      end

    end
  end
end
