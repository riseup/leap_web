module ReserveUsernames::Validate
  def self.included(base)
    base.class_eval do

      validate :check_if_address_is_taken

      protected

      #
      # Only query ReservedUsername if this is a new record.
      # Otherwise, validation will always fail because the address
      # has been taken... by this identity.
      #
      def check_if_address_is_taken
        if new_record? && address
          username = address.split('@').first
          if ReservedUsername.find(username)
            Rails.logger.info("ReserveUsernames - #{username} is taken.")
            errors.add :address, :taken
          end
        end
      rescue ActiveResource::ResourceNotFound
      end

    end
  end
end
