module ReserveUsernames
  CONFIG = YAML.load_file(File.expand_path('../../config.yml', __FILE__))[Rails.env]
end

require 'reserve_usernames/engine'
require 'reserve_usernames/callbacks'
require 'reserve_usernames/validate'

ActiveSupport.on_load(:identity) do
  include ReserveUsernames::Validate
  include ReserveUsernames::Callbacks
end
