$:.push File.expand_path("../lib", __FILE__)

require "reserve_usernames/version"

Gem::Specification.new do |s|
  s.name        = "reserve_usernames"
  s.version     = ReserveUsernames::VERSION
  s.authors     = ["LEAP"]
  s.summary     = "Connects to a external REST API in order to reserve usernames"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["README.md"]

  s.add_dependency "rails", "~> 4"
  s.add_dependency "activeresource", "~> 4"
end
